package lr1

import java.io.File
import java.util.*
import kotlin.collections.ArrayList

fun main() {
    val file = File("LR1_task1.txt")
    val scanner = Scanner(file)

    val out = ArrayList<Triple<Int, Int, Int>>()
    while (scanner.hasNextInt()) {
        val i = scanner.nextInt()
        val j = scanner.nextInt()

        out.add(Triple(i, j, findMaxLength(i, j)))
    }

    for (i in out) {
        println("${i.first} ${i.second} ${i.third}")
    }
}

fun findMaxLength(i: Int, j: Int): Int {
    var max = 0
    for (n in i..j) {
        max = kotlin.math.max(max, seqLength(n))
    }
    return max
}

fun seqLength(n: Int): Int {
    var n = n
    var l = 1
    while (n != 1) {
        n = if (n % 2 == 0)
            n / 2
        else n * 3 + 1
        l++
    }
    return l
}