package lr1

import java.io.File
import java.util.*
import kotlin.collections.ArrayList

fun main() {
    val file = File("LR1_task2.txt")
    val scanner = Scanner(file)

    var i = 1
    do {
        val n = scanner.nextInt()
        val m = scanner.nextInt()
        if (n > 0 && m > 0)
            scanner.nextLine()
        else break

        val field = ArrayList<ArrayList<Boolean>>()
        getField(field, n, m, scanner)
        printField(field, n, m, i++)
    } while (n > 0 && m > 0)
}

fun getField(field: ArrayList<ArrayList<Boolean>>, n: Int, m: Int, scanner: Scanner) {
    for (i in 0 until n) {
        field.add(ArrayList())
        val s = scanner.nextLine()
        for (j in 0 until m) {
            field[i].add(j, s[j] == '*')
        }
    }
}

fun checkCell(field: ArrayList<ArrayList<Boolean>>, i: Int, j: Int, n: Int, m: Int): Int {
    if (i in 0 until n && j in 0 until m) {
        return if (field[i][j]) 1
        else 0
    }
    return 0
}

fun findMines(field: ArrayList<ArrayList<Boolean>>, i: Int, j: Int, n: Int, m: Int): Int {
    var c = 0
    if (field[i][j])
        c = -1
    else {
        c += checkCell(field, i - 1, j, n, m)
        c += checkCell(field, i + 1, j, n, m)

        c += checkCell(field, i, j - 1, n, m)
        c += checkCell(field, i, j + 1, n, m)

        c += checkCell(field, i - 1, j - 1, n, m)
        c += checkCell(field, i - 1, j + 1, n, m)
        c += checkCell(field, i + 1, j - 1, n, m)
        c += checkCell(field, i + 1, j + 1, n, m)
    }
    return c
}

fun printField(field: ArrayList<ArrayList<Boolean>>, n: Int, m: Int, i: Int) {
    println("Field #$i")
    for (i in 0 until n) {
        for (j in 0 until m) {
            val n = findMines(field, i, j, n, m)
            print(
                if (n >= 0)
                    n
                else
                    "*"
            )
        }
        println()
    }
}